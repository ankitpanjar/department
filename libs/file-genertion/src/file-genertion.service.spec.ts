import { Test, TestingModule } from '@nestjs/testing';
import { FileGenertionService } from './file-genertion.service';

describe('FileGenertionService', () => {
  let service: FileGenertionService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FileGenertionService],
    }).compile();

    service = module.get<FileGenertionService>(FileGenertionService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
