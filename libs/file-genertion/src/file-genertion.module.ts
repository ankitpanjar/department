import { Module } from '@nestjs/common';
import { FileGenertionService } from './file-genertion.service';

@Module({
  providers: [FileGenerationService],
  exports: [FileGenerationService],
})
export class FileGenerationModule {}
