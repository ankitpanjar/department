import { Injectable } from '@nestjs/common';
import * as ExcelJS from 'exceljs';
import * as puppeteer from 'puppeteer';

@Injectable()
export class FileGenertionService {
  async generateTablePDF(data, fileName, columns) {
    const browser = await puppeteer.launch({ headless: 'new' });
    const page = await browser.newPage();

    // Create an HTML content to be converted to PDF
    const htmlContent = `
              <html>
                <head>
                  <style>
                    body {
                      font-family: Arial, sans-serif;
                    }
                    h1, h2 {
                      display: inline;
                    }
                    h1 {
                      font-size: 24px;
                    }
                    h2 {
                      font-size: 20px;
                    }
                    p {
                      font-size: 16px;
                    }
                    table {
                      width: 100%;
                    }
                    th, td {
                      padding: 10px;
                      text-align: center;
                    }
                    th {
                      background-color: #f2f2f2;
                    }
                    thead { display: table-header-group; }
                  </style>
                </head>
                <body>
                  <h1>SyncOffice</h1>
                  <h2>${fileName}</h2>
                  <h2>${new Date().toLocaleDateString()}</h2>
                  <table>
                    <thead>
                      <tr>
                        ${columns
                          .map((header) => `<th>${header}</th>`)
                          .join('')}
                      </tr>
                    </thead>
                    <tbody>
                      ${data
                        .map(
                          (row) => `
                        <tr>
                          ${columns
                            .map((column) => `<td>${row[column]}</td>`)
                            .join('')}
                        </tr>
                      `,
                        )
                        .join('')}
                    </tbody>
                  </table>
                </body>
              </html>
            `;

    await page.setContent(htmlContent);
    const pdfBuffer = await page.pdf({
      format: 'A4',
    });

    await browser.close();

    return pdfBuffer;
  }

  async generateDataInExcelFormate(
    requestedColumns: string[],
    tableName: string,
    data: any[],
  ): Promise<ExcelJS.Workbook> {
    const workbook = new ExcelJS.Workbook();
    const worksheet = workbook.addWorksheet(tableName);

    worksheet.addRow(requestedColumns);

    data.forEach((row) => {
      const rowData = requestedColumns.map((column) => row[column]);
      worksheet.addRow(rowData);
    });

    requestedColumns.forEach((column, columnIndex) => {
      const maxColumnWidth = Math.max(
        ...worksheet
          .getColumn(columnIndex + 1)
          .values.map((value) => (value ? value.toString().length : 0)),
      );
      worksheet.getColumn(columnIndex + 1).width = maxColumnWidth + 2;
    });

    return workbook;
  }

  generateCSVData(requestedColumns: string[], data: any[], tableName): string {
    const csvData = [];
    // Add header row to CSV
    csvData.push(requestedColumns);

    // Add data rows to CSV
    data.forEach((row) => {
      const rowData = requestedColumns.map((column) => row[column]);
      csvData.push(rowData);
    });

    // Convert the CSV data to a string
    const csvString = csvData.map((row) => row.join(',')).join('\n');
    return csvString;
  }
}
