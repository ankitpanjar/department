/*
  Warnings:

  - You are about to drop the `_m_admin_departmentTouser_auth` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE `_m_admin_departmentTouser_auth` DROP FOREIGN KEY `_m_admin_departmentTouser_auth_A_fkey`;

-- DropForeignKey
ALTER TABLE `_m_admin_departmentTouser_auth` DROP FOREIGN KEY `_m_admin_departmentTouser_auth_B_fkey`;

-- DropTable
DROP TABLE `_m_admin_departmentTouser_auth`;

-- AddForeignKey
ALTER TABLE `m_admin_department` ADD CONSTRAINT `m_admin_department_created_by_fkey` FOREIGN KEY (`created_by`) REFERENCES `user_auth`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `m_admin_department` ADD CONSTRAINT `m_admin_department_modified_by_fkey` FOREIGN KEY (`modified_by`) REFERENCES `user_auth`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
