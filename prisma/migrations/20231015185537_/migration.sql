-- DropForeignKey
ALTER TABLE `user_auth` DROP FOREIGN KEY `user_auth_m_admin_department_id_fkey`;

-- CreateTable
CREATE TABLE `_m_admin_departmentTouser_auth` (
    `A` INTEGER NOT NULL,
    `B` INTEGER NOT NULL,

    UNIQUE INDEX `_m_admin_departmentTouser_auth_AB_unique`(`A`, `B`),
    INDEX `_m_admin_departmentTouser_auth_B_index`(`B`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `_m_admin_departmentTouser_auth` ADD CONSTRAINT `_m_admin_departmentTouser_auth_A_fkey` FOREIGN KEY (`A`) REFERENCES `m_admin_department`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `_m_admin_departmentTouser_auth` ADD CONSTRAINT `_m_admin_departmentTouser_auth_B_fkey` FOREIGN KEY (`B`) REFERENCES `user_auth`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
