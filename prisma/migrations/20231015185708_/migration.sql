/*
  Warnings:

  - You are about to drop the column `m_admin_department_id` on the `user_auth` table. All the data in the column will be lost.

*/
-- DropIndex
DROP INDEX `user_auth_m_admin_department_id_fkey` ON `user_auth`;

-- AlterTable
ALTER TABLE `user_auth` DROP COLUMN `m_admin_department_id`;
