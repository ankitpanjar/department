/*
  Warnings:

  - Added the required column `created_by_username` to the `m_admin_department` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `m_admin_department` ADD COLUMN `created_by_username` VARCHAR(191) NOT NULL;
