/*
  Warnings:

  - You are about to drop the `_m_admin_departmentTouser_auth` table. If the table is not empty, all the data it contains will be lost.
  - Added the required column `m_admin_department_id` to the `user_auth` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE `_m_admin_departmentTouser_auth` DROP FOREIGN KEY `_m_admin_departmentTouser_auth_A_fkey`;

-- DropForeignKey
ALTER TABLE `_m_admin_departmentTouser_auth` DROP FOREIGN KEY `_m_admin_departmentTouser_auth_B_fkey`;

-- AlterTable
ALTER TABLE `user_auth` ADD COLUMN `m_admin_department_id` INTEGER NOT NULL;

-- DropTable
DROP TABLE `_m_admin_departmentTouser_auth`;

-- AddForeignKey
ALTER TABLE `user_auth` ADD CONSTRAINT `user_auth_m_admin_department_id_fkey` FOREIGN KEY (`m_admin_department_id`) REFERENCES `m_admin_department`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
