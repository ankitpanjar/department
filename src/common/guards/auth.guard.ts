import {
  CanActivate,
  ExecutionContext,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import * as jwt from 'jsonwebtoken';
import { AuthenticationService } from 'src/authentication/authentication.service';
import { PrismaService } from 'src/prisma/prisma.service';

interface JwtPayloadInterface {
  username: string;
  password: string;
  iat: number;
  exp: number;
}

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private readonly userRepository: PrismaService,
    private readonly authService: AuthenticationService,
    private reflector: Reflector,
  ) {}

  async canActivate(context: ExecutionContext) {
    const request = context.switchToHttp().getRequest();
    const jwtToken = request.headers?.authorization?.split(' ')[1];
    const isPublic = this.reflector.getAllAndOverride('isPublic', [
      context.getHandler(),
      context.getClass(),
    ]);

    if (isPublic) return true;
    try {
      const payload = (await jwt.decode(
        jwtToken,
      )) as unknown as JwtPayloadInterface;
      const user = await this.userRepository.user_auth.findUnique({
        where: { username: payload.username },
      });
      if (user.username !== payload.username) throw new NotFoundException();

      const isTokenExpired = await this.authService.isTokenExpired(jwtToken);

      if (isTokenExpired)
        throw new UnauthorizedException({ message: 'Jwt Token Expired' });

      return true;
    } catch (error) {
      return false;
    }
  }
}
