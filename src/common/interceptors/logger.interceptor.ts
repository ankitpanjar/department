import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { tap } from 'rxjs';
import { LoggerService } from 'src/common/logger/logger.service';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class LoggerInterceptor implements NestInterceptor {
  constructor(
    private readonly logger: LoggerService,
    private readonly prismaService: PrismaService,
  ) {}

  async intercept(context: ExecutionContext, next: CallHandler<any>) {
    const ctx = context.switchToHttp();
    const request = ctx.getRequest();
    const response = ctx.getResponse();
    const { method, originalUrl, ip } = request;
    // const args = context.getArgs();
    // const id = args[0].params.id;
    const startTime = Date.now();
    const userName = request.body.username;
    const statusCode = response.statusCode;

    return next.handle().pipe(
      tap(async () => {
        const endTime = Date.now();
        const resTime = endTime - startTime;
        const { statusCode } = response;
        const statusMessage = this.getStatusMessage(statusCode);

        // const forwardedFor = request.headers['x-forwarded-for'];
        // const realIp = request.headers['x-real-ip'];
        let userId = 1;
        let user;

        if (userName) {
          user = await this.prismaService.user_auth.findUnique({
            where: {
              username: userName,
            },
          });
        }

        if (user) {
          userId = user.id;
        }

        if (request.user !== null && request.user !== undefined) {
          userId = request.user.id;
          // console.log('request user', request.user);
          const user1 = await this.prismaService.user_auth.findUnique({
            where: {
              username: request.user.username,
            },
          });
          userId = user1.id;
        }
        const dateTime = new Date();
        const logLevel = 'info';
        const userExists = await this.prismaService.user_auth.findUnique({
          where: {
            id: userId,
          },
        });
        if (!userExists) {
          throw new Error(`User with id ${userId} does not exist.`);
        }
        try {
          const adminLogs = await this.prismaService.m_admin_logs.create({
            data: {
              user_id: userId,
              http_operation: method,
              log_type: logLevel,
              time_stamp: dateTime,
              status: statusCode,
            },
          });
        } catch (error) {
          console.error('Error creating admin logs:', error);
          throw error;
        }
        // Create a structured log entry in JSON format
        const logEntry = {
          timestamp: dateTime.toISOString(),
          method: method,
          url: originalUrl,
          client_ip: ip,
          response: {
            status_code: statusCode,
            status_message: statusMessage,
            response_time: resTime,
          },
        };

        // Log the information in the desired format
        const logMessage = `Incoming request: timestamp:${logEntry.timestamp},method:${logEntry.method},url:${logEntry.url},Ip:${logEntry.client_ip}, Outgoing response: status_code:${logEntry.response.status_code},status_message:${logEntry.response.status_message},response_time:${logEntry.response.response_time}ms`;
        this.logger.log(logMessage);
      }),
    );
  }

  private getStatusMessage(statusCode: number): string {
    // Define a mapping of status codes to messages
    const statusMessages: { [key: number]: string } = {
      200: 'OK',
      201: 'User Created Successfully',
      202: 'User SignIn Successfully',
    };

    return statusMessages[statusCode] || 'Unknown';
  }
}
