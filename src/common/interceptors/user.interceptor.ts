import {
  CallHandler,
  ExecutionContext,
  NestInterceptor,
  UnauthorizedException,
} from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import * as jwt from 'jsonwebtoken';

interface jwtpayloadInterface {
  jwtToken: any;
  email: string;
  password: string;
  iat: number;
  exp: number;
}

export class UserInterceptor implements NestInterceptor {
  constructor(private readonly prismaService: PrismaService) {}

  async intercept(
    context: ExecutionContext,
    next: CallHandler<any>,
  ): Promise<any> {
    const request = context.switchToHttp().getRequest();
    const jwtToken = request?.headers?.authorization?.split(' ')[1];
    // console.log({ jwtToken });
    const user = (await jwt.decode(jwtToken)) as jwtpayloadInterface;
    request.user = user;
    return next.handle();
  }
}
