import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
} from '@nestjs/common';
import { LoggerService } from './logger.service';
import { PrismaService } from 'src/prisma/prisma.service';

@Catch(HttpException)
export class HttpErrorFilter implements ExceptionFilter {
  constructor(
    private readonly logger: LoggerService,
    private readonly prismaService: PrismaService,
  ) {}

  async catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const request = ctx.getRequest();
    const response = ctx.getResponse();
    const status = exception.getStatus();
    const { method, originalUrl, ip } = request;
    const statusMessage = exception.message;
    const exceptionResponse = exception.getResponse();

    const timestamp = new Date().toISOString();
    const responseTime = Date.now() - request['_timestamp'];

    const logEntry = {
      timestamp: timestamp,
      method: method,
      url: originalUrl,
      user_ip: ip,
      response: {
        status_code: status,
        status_message: statusMessage,
        response_time: responseTime,
      },
    };

    const userName = request.body.username;
    const logMessage = `Incoming request: timestamp:${logEntry.timestamp},method:${logEntry.method},url:${logEntry.url},Ip:${logEntry.user_ip}, Outgoing response: status_code:${logEntry.response.status_code},status_message:${logEntry.response.status_message},response_time:${logEntry.response.response_time}`;
    this.logger.error(logMessage);
    // const statusMessage = getStatusMessage(statusCode);
    // const forwardedFor = request.headers['x-forwarded-for'];
    // const realIp = request.headers['x-real-ip'];
    let userId = 1;
    let user;
    if (userName) {
      user = await this.prismaService.user_auth.findUnique({
        where: {
          username: userName,
        },
      });
    }
    if (user) {
      userId = user.id;
    }
    if (request.user !== null && request.user !== undefined) {
      userId = request.user.id;
      const employee1 = await this.prismaService.user_auth.findUnique({
        where: {
          username: request.user.username,
        },
      });
      userId = employee1.id;
    }
    const dateTime = new Date();
    const logLevel = 'error';
    const userExists = await this.prismaService.user_auth.findUnique({
      where: {
        id: userId,
      },
    });
    if (!userExists) {
      throw new Error(`User with id ${userId} does not exist.`);
    }
    const errorResponse =
      // timestamp: timestamp,
      // method: method,
      // url: originalUrl,
      // user_ip: ip,
      exceptionResponse;
    // };

    try {
      const adminLogs = await this.prismaService.m_admin_logs.create({
        data: {
          user_id: userId,
          http_operation: method,
          log_type: logLevel,
          time_stamp: dateTime,
          status: status,
        },
      });
    } catch (error) {
      console.error('Error creating admin logs:', error);
      throw error;
    }
    response.status(status).json(errorResponse);
  }
}

// function getStatusMessage(statusCode: number): string {
//   // Define a mapping of status codes to messages
//   const statusMessages: { [key: number]: string } = {
//     400: 'Bad Request',
//     401: 'unauthorized',
//     403: 'Forbidden',
//     404: 'Not Found',
//     406: 'Not Acceptable',
//     408: 'Request Timeout',
//   };
// return statusMessages[statusCode] || 'Unknown';
// }
