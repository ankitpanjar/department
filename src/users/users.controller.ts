import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import * as _ from 'lodash';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiQuery,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { UsersService } from './users.service';
import { CreateUserDto, searchModuleDto } from './dtos/create-user.dto';
import { UpdateAdminDto } from './dtos/update-user.dto';
import { AuthGuard } from 'src/common/guards/auth.guard';
import { LoggerService } from 'src/common/logger/logger.service';
// import { FileInterceptor } from '@nestjs/platform-express';

@ApiTags('User')
@Controller('users')
export class UsersController {
  constructor(
    private readonly userService: UsersService,
    private readonly loggerService: LoggerService,
  ) {}

  @ApiQuery({
    name: 'username',
    description: 'The name of the user',
    type: CreateUserDto,
  })
  @ApiCreatedResponse({
    description: 'Create user',
    type: CreateUserDto,
  })
  @ApiBadRequestResponse({
    description: 'User cannot created, Try again',
  })
  @Post()
  @UseInterceptors()
  async createModule(@Body() createUserDto: CreateUserDto) {
    return await this.userService.createUser(createUserDto);
  }

  @ApiQuery({
    description: 'Api to get all user',
  })
  @ApiBearerAuth('JWT-Auth')
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @Get()
  @UseGuards(AuthGuard)
  async getAllData() {
    const res = await this.userService.findAll();
    return res;
  }

  @ApiQuery({
    description: 'API to Filter',
    type: searchModuleDto,
    required: false,
  })
  @Get('getModuleLists')
  async getModules(@Query() pagenationDto: searchModuleDto) {
    if (_.isEmpty(pagenationDto)) {
      // return await this.userService.getModules(pagenationDto);
    } else if (pagenationDto['advanceFilter']) {
      console.log(pagenationDto);
      // return await this.userService.getModules(pagenationDto);
    } else {
      // return await this.userService.getModules(pagenationDto);
    }
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.userService.findOne(+id);
  }

  @ApiQuery({
    description: 'API to Update Modules',
    type: UpdateAdminDto,
    required: false,
  })
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateAdminDto: UpdateAdminDto) {
    return this.userService.update(+id, updateAdminDto);
  }

  @ApiQuery({
    description: 'API to Delete Modules',
    name: 'Id',
    required: false,
  })
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.userService.remove(+id);
  }
}
