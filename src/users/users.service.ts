import { Injectable, NotAcceptableException } from '@nestjs/common';
import { UpdateAdminDto } from './dtos/update-user.dto';
import { PrismaService } from 'src/prisma/prisma.service';
import { CreateUserDto } from './dtos/create-user.dto';
import * as bcrypt from 'bcryptjs';

@Injectable()
export class UsersService {
  constructor(private readonly moduleRepository: PrismaService) {}

  async checkEmployeeByEmail(username: string) {
    const user = await this.moduleRepository.user_auth.findUnique({
      where: {
        username,
      },
    });
    return user;
  }

  async createUser(body: CreateUserDto) {
    const userEmail = await this.checkEmployeeByEmail(body.username);
    if (userEmail)
      throw new NotAcceptableException({
        message: `Email ${body.username} already Exist.`,
      });
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(body.password, salt);
    const res = await this.moduleRepository.user_auth.create({
      data: {
        username: body.username,
        password: hashedPassword,
        salt,
      },
    });
    const user = await this.moduleRepository.user_auth.findUnique({
      where: {
        id: res.id,
      },
      select: {
        id: true,
        username: true,
      },
    });
    return user;
  }

  async findAll() {
    return await this.moduleRepository.user_auth.findMany();
  }

  findOne(id: number) {
    return `This action returns a #${id} admin`;
  }

  update(id: number, updateAdminDto: UpdateAdminDto) {
    return `This action updates a #${id} admin`;
  }

  remove(id: number) {
    return `This action removes a #${id} admin`;
  }
}
