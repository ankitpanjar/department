import {
  IsBoolean,
  IsDate,
  IsObject,
  IsOptional,
  IsPositive,
  IsString,
} from 'class-validator';
import { Expose, Transform } from 'class-transformer';
import { ApiProperty, ApiQuery } from '@nestjs/swagger';

export class searchModuleDto {
  @ApiProperty({ required: false })
  @IsOptional()
  @IsPositive()
  page?: number;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsPositive()
  limit?: number;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsString()
  search?: string;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsString()
  column_sort?: string;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsString()
  sort_order?: 'ASC' | 'DESC';

  @IsOptional()
  @IsObject()
  advanceFilter: object;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsString()
  name: string;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsString()
  description: string;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsString()
  code: string;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsDate()
  created_on: Date;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsDate()
  modified_on: Date;

  @ApiProperty({ required: false })
  @Expose()
  @Transform((value) => (value ? 1 : 0))
  @IsBoolean()
  status: string;
}

export class CreateUserDto {
  @ApiProperty({
    required: true,
    description: 'The name of the user',
    // example: 'username@gmail.com',
  })
  @IsString()
  // @ApiQuery({ name: 'username' }) // Add this line
  username: string;

  @ApiProperty({
    required: true,
    description: 'password',
    // example: 'password',
  })
  // @ApiQuery({ name: 'password' }) // Add this line
  @IsString()
  password: string;
}
