import { PartialType } from '@nestjs/mapped-types';
import { searchModuleDto } from './create-user.dto';

export class UpdateAdminDto extends PartialType(searchModuleDto) {}
