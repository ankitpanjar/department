import { Test, TestingModule } from '@nestjs/testing';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { CreateUserDto } from './dtos/create-user.dto';

describe('UsersController', () => {
  let controller: UsersController;
  let service: UsersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [UsersService],
    }).compile();

    controller = module.get<UsersController>(UsersController);
    service = module.get<UsersService>(UsersService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create a new user', async () => {
    // Create a sample user data
    // const createUserDto: CreateUserDto = {
    //   username: 'newuser@example.com',
    //   password: 'password123',
    // };

    // Mock the service method to return a resolved promise with the user data
    const createdUser = {
      id: 1,
      username: 'newuser@example.com',
      password: 'password123',
      // Add other properties as needed
    };
    // jest.spyOn(service, 'createUser').mockResolvedValue(createdUser);

    // Simulate a request to the controller
    // const response = await controller.createModule(createUserDto);

    // Assert the response and status code
    // expect(response).toEqual(createdUser);
  });

  it('should get a list of users', async () => {
    // Write a test case to get a list of users using the controller and service
    // Mock any dependencies, similar to how you did in the UsersService test
  });

  // Add more test cases for other controller actions as needed
});
