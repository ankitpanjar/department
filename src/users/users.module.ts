import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { AuthenticationService } from 'src/authentication/authentication.service';
import { JwtService } from '@nestjs/jwt';
import { PrismaModule } from 'src/prisma/prisma.module';
import { UsersController } from './users.controller';
import { RedisModule } from 'src/redis/redis.module';
import { PrismaService } from 'src/prisma/prisma.service';
import { LoggerService } from 'src/common/logger/logger.service';

@Module({
  imports: [PrismaModule, RedisModule],
  controllers: [UsersController],
  providers: [
    UsersService,
    AuthenticationService,
    JwtService,
    PrismaService,
    LoggerService,
  ],
  exports: [UsersService],
})
export class UsersModule {}
