// import { Test, TestingModule } from '@nestjs/testing';
// import { PrismaService } from './prisma.service';

// describe('PrismaService', () => {
//   let service: PrismaService;

//   beforeEach(async () => {
//     const module: TestingModule = await Test.createTestingModule({
//       providers: [PrismaService],
//     }).compile();

//     service = module.get<PrismaService>(PrismaService);
//   });

//   it('should be defined', () => {
//     expect(service).toBeDefined();
//   });
// });
import { Test, TestingModule } from '@nestjs/testing';
import { PrismaService } from './prisma.service';
import { PrismaClient } from '@prisma/client';

describe('PrismaService', () => {
  let service: PrismaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PrismaService,
        {
          provide: PrismaClient, // Replace with the actual PrismaClient token
          useValue: {
            // Mock or stub methods you use in PrismaService here
            // For example, if PrismaService uses user.findUnique, you can mock it.
            user: {
              findUnique: jest.fn(),
              // Mock other methods as needed
            },
          },
        },
      ],
    }).compile();

    service = module.get<PrismaService>(PrismaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  // Add more test cases specific to your PrismaService if needed
});
