import { Test, TestingModule } from '@nestjs/testing';
import { DepartmentService } from './department.service';
import { PrismaService } from 'src/prisma/prisma.service';
import { NotFoundException, ConflictException } from '@nestjs/common';
import { SearchDepartmentDto } from './dtos/serarchDepartment.dto';
import { ResponseDepartmentDto } from './dtos/responseDepartment.dto';

describe('DepartmentService', () => {
  let departmentService: DepartmentService;
  let prismaService: PrismaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DepartmentService, PrismaService],
    }).compile();

    departmentService = module.get<DepartmentService>(DepartmentService);
    prismaService = module.get<PrismaService>(PrismaService);
  });

  it('should be defined', () => {
    expect(departmentService).toBeDefined();
  });

  describe('createDepartmentMethod', () => {
    it('should create a department', async () => {
      const body = {
        name: 'arun',
        description: 'backend developer',
        code: 'arun123',
      };
      const user = { id: 1 }; // Mock user data
      const createDepartmentData = {
        id: 1,
        name: body.name,
        description: body.description,
        code: body.code,
        status: true,
        created_on: new Date(),
        modified_on: new Date(),
        created_by: user.id,
        modified_by: user.id,
      };

      jest
        .spyOn(prismaService.m_admin_department, 'create')
        .mockResolvedValue(createDepartmentData);

      const result = await departmentService.createDepartmentMethod(body, user);

      expect(result).toEqual(new ResponseDepartmentDto(createDepartmentData));
    });

    it('should handle conflict when department already exists', async () => {
      const body = {
        name: 'Test Department',
        description: 'Test Department Description',
        code: 'DEPT001',
      };
      const user = { id: 1 };

      jest
        .spyOn(prismaService.m_admin_department, 'create')
        .mockRejectedValue(new ConflictException('Already Exist'));

      try {
        await departmentService.createDepartmentMethod(body, user);
      } catch (error) {
        expect(error).toBeInstanceOf(ConflictException);
        expect(error.message).toBe('something went wrong');
      }
    });
  });

  describe('getAllDepartmentListsMethod', () => {
    it('should get all department lists', async () => {
      // Mock the data you expect to receive from the PrismaService
      const expectedData = [
        {
          id: 1,
          name: 'Test Department 1',
          description: 'Description 1',
          code: 'DEPT001',
          status: true,
          created_on: new Date(),
          modified_on: new Date(),
          created_by: 1,
          modified_by: 1,
        },
        {
          id: 2,
          name: 'Test Department 2',
          description: 'Description 2',
          code: 'DEPT002',
          status: true,
          created_on: new Date(),
          modified_on: new Date(),
          created_by: 1,
          modified_by: 1,
        },
      ];

      jest
        .spyOn(prismaService.m_admin_department, 'findMany')
        .mockResolvedValue(expectedData);
      jest
        .spyOn(prismaService.m_admin_department, 'count')
        .mockResolvedValue(expectedData.length);

      const queryDto: SearchDepartmentDto = {
        offset: 1,
        limit: '10',
        search: '',
        column_sort: '',
        sort_order: 'ASC',
        advanceFilter: {}, // Add your advanceFilter value
        name: '',
        description: '',
        code: '',
        status: '',
      };

      const result = await departmentService.getAllDepartmentListsMethod(
        queryDto,
      );

      expect(result.data.length).toBe(expectedData.length);
      expect(result.total).toBe(expectedData.length);
    });

    it('should handle conflict when an error occurs', async () => {
      jest
        .spyOn(prismaService.m_admin_department, 'findMany')
        .mockRejectedValue(new ConflictException());

      const queryDto: SearchDepartmentDto = {
        offset: 1,
        limit: '10',
        search: '',
        column_sort: '',
        sort_order: 'ASC',
        advanceFilter: {}, // Add your advanceFilter value
        name: '',
        description: '',
        code: '',
        status: '',
      };

      try {
        await departmentService.getAllDepartmentListsMethod(queryDto);
      } catch (error) {
        expect(error).toBeInstanceOf(ConflictException);
      }
    });
  });

  describe('getDepartmentListByidMethod', () => {
    it('should get a department by ID', async () => {
      const departmentId = 1; // Provide a valid department ID
      // Mock the data you expect to receive from the PrismaService
      const expectedData = {
        id: departmentId,
        name: 'Test Department',
        description: 'Test Department Description',
        code: 'DEPT001',
        status: true,
        created_on: new Date(),
        modified_on: new Date(),
        created_by: 1,
        modified_by: 1,
      };

      jest
        .spyOn(prismaService.m_admin_department, 'findUnique')
        .mockResolvedValue(expectedData);

      const result = await departmentService.getDepartmentListByidMethod(
        departmentId,
      );

      expect(result.id).toBe(departmentId);
    });

    it('should handle conflict when an error occurs', async () => {
      const departmentId = 1; // Provide a valid department ID

      jest
        .spyOn(prismaService.m_admin_department, 'findUnique')
        .mockRejectedValue(new ConflictException());

      try {
        await departmentService.getDepartmentListByidMethod(departmentId);
      } catch (error) {
        expect(error).toBeInstanceOf(ConflictException);
      }
    });
  });

  describe('updateDepartmentListByIdMethod', () => {
    it('should update a department by ID', async () => {
      const departmentId = 19; // Provide a valid department ID
      const body = {
        name: 'sumit',
        description: 'Updated Department Description',
        code: 'DEPT002',
      };
      const user = { id: 1 }; // Mock user data
      // Mock the data you expect to receive from the PrismaService
      const expectedData = {
        id: departmentId,
        name: body.name,
        description: body.description,
        code: body.code,
        status: true,
        created_on: new Date(),
        modified_on: new Date(),
        created_by: user.id,
        modified_by: user.id,
      };

      jest
        .spyOn(prismaService.m_admin_department, 'update')
        .mockResolvedValue(expectedData);

      const result = await departmentService.updateDepartmentListByIdMethod(
        departmentId,
        body,
        user,
      );

      expect(result.name).toBe(body.name);
    });

    it('should handle conflict when an error occurs', async () => {
      const id = 2; // Provide a valid department ID
      const body = {
        name: 'Updated Department',
        description: 'Updated Department Description',
        code: 'DEPT002',
      };
      const user = { id: 1 };

      jest
        .spyOn(prismaService.m_admin_department, 'update')
        .mockRejectedValue(new ConflictException('Already Exist'));

      try {
        await departmentService.updateDepartmentListByIdMethod(id, body, user);
      } catch (error) {
        expect(error).toBeInstanceOf(NotFoundException); // Change to ConflictException
        expect(error.message).toBe('Department id 2 is not Exist in database'); // Change the expected error message
      }
    });
  });

  describe('activateDepartmentListByIdMethod', () => {
    it('should activate a department by ID', async () => {
      const departmentId = 1; // Provide a valid department ID

      // Mock the data you expect to receive from the PrismaService
      const departmentData = {
        id: departmentId,
        name: 'Test Department',
        description: 'Test Department Description',
        code: 'DEPT001',
        status: false, // Department is initially activated
        created_on: new Date(),
        modified_on: new Date(),
        created_by: 1,
        modified_by: 1,
      };

      jest
        .spyOn(prismaService.m_admin_department, 'findUnique')
        .mockResolvedValue(departmentData); // Simulate department with ID 2 exists

      // Mock the checkDepartmentById function to return the same department data
      jest
        .spyOn(departmentService, 'checkDepartmentById')
        .mockResolvedValue(departmentData);

      // Mock the update function to set the department as activated (if necessary)
      const updatedDepartmentData = {
        ...departmentData,
        status: true, // Ensure the status is true to simulate activation
        modified_on: new Date(),
        modified_by: 1,
      };

      jest
        .spyOn(prismaService.m_admin_department, 'update')
        .mockResolvedValue(updatedDepartmentData);

      const user = { id: 1 }; // Mock user data

      const result = await departmentService.activateDepartmentListByIdMethod(
        departmentId,
        user,
      );
      expect(result.id).toBe(departmentId);
    });

    it('should handle conflict when the department is already activated', async () => {
      const id = 2;
      const user = { id: 1 }; // Mock user data
      const expectedData = {
        id: id,
        name: 'Test Department',
        description: 'Test Department Description',
        code: 'DEPT001',
        status: true, // Department is already activated
        created_on: new Date(),
        modified_on: new Date(),
        created_by: user.id,
        modified_by: user.id,
      };

      jest
        .spyOn(prismaService.m_admin_department, 'findUnique')
        .mockResolvedValue(expectedData);

      try {
        await departmentService.activateDepartmentListByIdMethod(id, user);
      } catch (error) {
        expect(error).toBeInstanceOf(ConflictException);
        expect(error.message).toBe(
          `department ${expectedData.name} is already activated`,
        );
      }
    });
  });

  describe('deleteDepartmentListByIdMethod', () => {
    it('should soft delete a department by ID', async () => {
      const departmentId = 2; // Provide a valid department ID

      // Mock the data you expect to receive from the PrismaService
      const departmentData = {
        id: departmentId,
        name: 'Test Department',
        description: 'Test Department Description',
        code: 'DEPT001',
        status: true, // Department is initially active
        created_on: new Date(),
        modified_on: new Date(),
        created_by: 1,
        modified_by: 1,
      };

      jest
        .spyOn(prismaService.m_admin_department, 'findUnique')
        .mockResolvedValue(departmentData); // Simulate department with ID 2 exists

      // Mock the update function to set the department as deleted
      const updatedDepartmentData = {
        ...departmentData,
        status: false, // Department is now soft deleted
        modified_on: new Date(),
        modified_by: 1,
      };

      jest
        .spyOn(prismaService.m_admin_department, 'update')
        .mockResolvedValue(updatedDepartmentData);

      const user = { id: 1 }; // Mock user data

      const result = await departmentService.deleteDepartmentListByIdMethod(
        departmentId,
        user,
      );
      expect(result.id).toBe(departmentId);
    });

    it('should handle conflict when the department is already deleted', async () => {
      const id = 1; // Provide a valid department ID
      const user = { id: 1 }; // Mock user data
      // Mock the data you expect to receive from the PrismaService
      const expectedData = {
        id: id,
        name: 'Test Department',
        description: 'Test Department Description',
        code: 'DEPT001',
        status: false, // Department is already soft deleted
        created_on: new Date(),
        modified_on: new Date(),
        created_by: user.id,
        modified_by: user.id,
      };

      jest
        .spyOn(prismaService.m_admin_department, 'findUnique')
        .mockResolvedValue(expectedData);

      try {
        await departmentService.deleteDepartmentListByIdMethod(id, user);
      } catch (error) {
        expect(error).toBeInstanceOf(ConflictException);
        expect(error.message).toBe(`${id} already deleted`);
      }
    });
  });

  describe('checkDepartmentById', () => {
    it('should check if a department exists by ID', async () => {
      const departmentId = 1; // Provide a valid department ID
      // Mock the data you expect to receive from the PrismaService
      const expectedData = {
        id: departmentId,
        name: 'Test Department',
        description: 'Test Department Description',
        code: 'DEPT001',
        status: true,
        created_on: new Date(),
        modified_on: new Date(),
        created_by: 1,
        modified_by: 1,
      };

      jest
        .spyOn(prismaService.m_admin_department, 'findUnique')
        .mockResolvedValue(expectedData);

      const result = await departmentService.checkDepartmentById(departmentId);

      expect(result.id).toBe(departmentId);
    });

    it('should handle not found error when the department does not exist', async () => {
      const departmentId = 1; // Provide an invalid department ID

      jest
        .spyOn(prismaService.m_admin_department, 'findUnique')
        .mockResolvedValue(null);

      try {
        await departmentService.checkDepartmentById(departmentId);
      } catch (error) {
        expect(error).toBeInstanceOf(NotFoundException);
      }
    });
  });
});
