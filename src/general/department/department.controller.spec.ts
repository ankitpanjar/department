import { Test, TestingModule } from '@nestjs/testing';
import { DepartmentController } from './department.controller';
import { DepartmentService } from './department.service';
import { CreateDepartmentDto } from './dtos/createDepartment.dto';
import { PrismaService } from 'src/prisma/prisma.service';
import { SearchDepartmentDto } from './dtos/serarchDepartment.dto';
import { AuthenticationService } from 'src/authentication/authentication.service';
import { Reflector } from '@nestjs/core';
import { JwtService } from '@nestjs/jwt';
import { RedisService } from 'src/redis/redis.service';
import { RedisModule } from 'src/redis/redis.module';

const mockGetDepartmentData = {
  id: 1,
  name: 'Balaji',
  description: 'Developer',
  code: '004455',
  status: 'active',
  created_by: 1,
  modified_by: 1,
  created_on: '2023-10-11T14:58:40.861Z',
  modified_on: '2023-10-11T14:58:40.861Z',
};

const mockUser = {
  id: 1, // Example user ID
  username: 'raja',
  // Add other user properties if needed
};

const mockCreateParam = {
  id: 1,
  name: 'Balaji',
  description: 'Developer',
  code: '004455',
};

describe('DepartmentController', () => {
  let controller: DepartmentController;
  let service: DepartmentService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DepartmentController],
      imports: [RedisModule],
      providers: [
        DepartmentService,
        PrismaService,
        AuthenticationService,
        RedisService,
        JwtService,
        Reflector,
      ],
    }).compile();

    controller = module.get<DepartmentController>(DepartmentController);
    service = module.get<DepartmentService>(DepartmentService);
  });

  describe('getAllDepartmentLists', () => {
    it('should get all department List', async () => {
      const mockGetAllDepartmentLists = jest
        .fn()
        .mockReturnValue({ data: [], total: 0 });
      jest
        .spyOn(service, 'getAllDepartmentListsMethod')
        .mockImplementation(mockGetAllDepartmentLists);
      const mockPaginationQuery: SearchDepartmentDto = {
        advanceFilter: undefined,
        name: '',
        description: '',
        code: '',
        status: '',
      };

      const result = await controller.getAllDepartmentLists(
        mockPaginationQuery,
      );

      expect(mockGetAllDepartmentLists).toHaveBeenCalledWith(
        mockPaginationQuery,
      );
      expect(result).toEqual({ data: [], total: 0 });
    });
  });

  describe('getDepartmentListById', () => {
    it('get department details by id', async () => {
      const mockGetDepartmentListsById = jest
        .fn()
        .mockReturnValue(mockGetDepartmentData);
      jest
        .spyOn(service, 'getDepartmentListByidMethod')
        .mockImplementation(mockGetDepartmentListsById);

      const result = await controller.getDepartmentListById(1);

      expect(mockGetDepartmentListsById).toHaveBeenCalledWith(1);
      expect(result).toEqual(mockGetDepartmentData);
    });
  });

  describe('createDepartment', () => {
    it('should create a department and return the result', async () => {
      const mockData: CreateDepartmentDto = {
        // id: 1,
        name: 'Balaji',
        description: 'Developer',
        code: '004455',
        // Add other properties as needed
      };

      service.createDepartmentMethod = jest
        .fn()
        .mockResolvedValue(mockGetDepartmentData);

      const result = await controller.createDepartment(mockData, mockUser);

      expect(result).toEqual(mockGetDepartmentData);
      expect(service.createDepartmentMethod).toHaveBeenCalledWith(
        mockData,
        mockUser,
      );
    });
  });

  describe('updateDepartmentListById', () => {
    it('update Department List', async () => {
      const mockUpdateDepartmentList = jest
        .fn()
        .mockReturnValue(mockGetDepartmentData);
      jest
        .spyOn(service, 'updateDepartmentListByIdMethod')
        .mockImplementation(mockUpdateDepartmentList);

      const result = await controller.updateDepartmentListById(
        1,
        mockCreateParam,
        mockUser,
      );

      expect(mockUpdateDepartmentList).toHaveBeenCalledWith(
        1,
        mockCreateParam,
        mockUser,
      );
      expect(result).toEqual(mockGetDepartmentData);
    });
  });

  describe('activateDepartmentListById', () => {
    it('activate Department List', async () => {
      const mockActivateDepartmentList = jest
        .fn()
        .mockReturnValue(mockGetDepartmentData);
      jest
        .spyOn(service, 'activateDepartmentListByIdMethod')
        .mockImplementation(mockActivateDepartmentList);

      const result = await controller.activateDepartmentListById(1, mockUser);

      expect(mockActivateDepartmentList).toHaveBeenCalledWith(1, mockUser);
      expect(result).toEqual(mockGetDepartmentData);
    });
  });

  describe('deleteDepartmentListById', () => {
    it('soft delete Department', async () => {
      const mockDeleteDepartment = jest.fn().mockReturnValue({ id: 1 });
      jest
        .spyOn(service, 'deleteDepartmentListByIdMethod')
        .mockImplementation(mockDeleteDepartment);

      const result = await controller.deleteDepartmentListById(1, mockUser);

      expect(mockDeleteDepartment).toHaveBeenCalledWith(1, mockUser);
      expect(result).toStrictEqual({ id: 1 });
    });
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
