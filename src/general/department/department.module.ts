import { Module } from '@nestjs/common';
import { DepartmentService } from './department.service';
import { DepartmentController } from './department.controller';
import { PrismaService } from 'src/prisma/prisma.service';
import { JwtService } from '@nestjs/jwt';
import { LoggerService } from 'src/common/logger/logger.service';
import { PrismaModule } from 'src/prisma/prisma.module';
import { AuthenticationModule } from 'src/authentication/authentication.module';
import { AuthenticationService } from 'src/authentication/authentication.service';
import { RedisService } from 'src/redis/redis.service';
import { RedisModule } from 'src/redis/redis.module';
// import { FileGenerationModule } from 'src/file-generation/file-generation.module';
// import { FileGenerationService } from 'src/file-generation/file-generation.service';
import { FileGenertionModule, FileGenertionService } from '@app/file-genertion';

@Module({
  imports: [
    PrismaModule,
    AuthenticationModule,
    RedisModule,
    FileGenertionModule,
  ],
  providers: [
    DepartmentService,
    PrismaService,
    JwtService,
    LoggerService,
    AuthenticationService,
    RedisService,
    FileGenertionService,
  ],
  controllers: [DepartmentController],
})
export class DepartmentModule {}
