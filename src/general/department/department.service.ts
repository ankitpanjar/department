import {
  Injectable,
  NotFoundException,
  ConflictException,
} from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { ResponseDepartmentDto } from './dtos/responseDepartment.dto';
import { UpdateDepartmentDto } from './dtos/updateDepartment.dto';
import { SearchDepartmentDto } from './dtos/serarchDepartment.dto';
import { FileGenertionService } from '@app/file-genertion';

const selectData = {
  id: true,
  name: true,
  description: true,
  code: true,
  status: true,
  created_on: true,
  modified_on: true,
  created_by: true,
  modified_by: true,
  created_user: { select: { username: true } },
  modified_user: { select: { username: true } },
};

@Injectable()
export class DepartmentService {
  constructor(
    private readonly prismaService: PrismaService,
    private readonly fileGeneration: FileGenertionService,
  ) {}

  // Create Department method
  async createDepartmentMethod(body, user) {
    try {
      const resultCreateDepartmentList =
        await this.prismaService.m_admin_department.create({
          data: {
            name: body.name,
            description: body.description,
            code: body.code,
            created_by: user.id,
            modified_by: user.id,
          },
          select: { ...selectData },
        });
      const returnData = new ResponseDepartmentDto(resultCreateDepartmentList);
      return returnData;
    } catch (error) {
      if (error.code === 'P2002' && error.meta.target.includes('name')) {
        throw new ConflictException('name already exist');
      } else if (error.code === 'P2002' && error.meta.target.includes('code')) {
        throw new ConflictException('code already exist');
      } else {
        throw new ConflictException('something went wrong');
      }
    }
  }

  // get all department lists
  async getAllDepartmentListsMethod(
    PaginationQueryDto: SearchDepartmentDto,
  ): Promise<{
    data: ResponseDepartmentDto[];
    total: number;
  }> {
    const limit: number = PaginationQueryDto.limit
      ? parseInt(PaginationQueryDto.limit)
      : 50;
    const {
      offset = 1,
      search,
      column_sort = 'created_on',
      sort_order = 'desc',
      name,
      description,
      code,
      status,
    } = PaginationQueryDto;
    let statusValue;
    if (status === '0') {
      statusValue = false;
    }
    if (status === '1') {
      statusValue = true;
    }
    const skip = (offset - 1) * limit;
    try {
      const where = {
        ...(search
          ? {
              OR: [
                {
                  name: { contains: search },
                },
                {
                  description: { contains: search },
                },
                {
                  code: { contains: search },
                },
              ],
            }
          : {}),
        ...(name ? { name: { contains: name } } : {}),
        ...(description ? { description: { contains: description } } : {}),
        ...(code ? { code: { contains: code } } : {}),
        status: statusValue,
      };
      const orderBy = {};
      if (column_sort && sort_order) {
        orderBy[column_sort] = sort_order;
      }

      const getAllDepartment =
        await this.prismaService.m_admin_department.findMany({
          where,
          select: {
            ...selectData,
          },
          orderBy,
          skip,
          take: limit,
        });
      const total = await this.prismaService.m_admin_department.count({
        where,
        orderBy,
        skip,
        take: limit,
      });
      const data = getAllDepartment.map(
        (department) => new ResponseDepartmentDto(department),
      );
      return { data, total };
    } catch (error) {
      throw new ConflictException('something went wrong');
    }
  }

  // get Department List by Id
  async getDepartmentListByidMethod(id) {
    await this.checkDepartmentById(id);
    try {
      const getDepartment =
        await this.prismaService.m_admin_department.findUnique({
          where: {
            id,
          },
          select: {
            ...selectData,
          },
        });
      return new ResponseDepartmentDto(getDepartment);
    } catch (error) {
      throw new ConflictException();
    }
  }

  // update Department List by Id
  async updateDepartmentListByIdMethod(
    id: number,
    body: UpdateDepartmentDto,
    user,
  ) {
    await this.checkDepartmentById(id);
    try {
      const result = await this.prismaService.m_admin_department.update({
        where: {
          id,
        },
        data: {
          name: body.name,
          description: body.description,
          code: body.code,
          modified_by: user.id,
        },
        select: {
          ...selectData,
        },
      });
      const data = new ResponseDepartmentDto(result);
      return data;
    } catch (error) {
      throw new ConflictException('Already Exist');
    }
  }

  // activate DepartmentList Method
  async activateDepartmentListByIdMethod(id, user): Promise<{ id: number }> {
    const departmentCheck = await this.checkDepartmentById(id);
    if (departmentCheck.status === true) {
      throw new ConflictException(
        `department ${departmentCheck.name} is already activated`,
      );
    }
    try {
      await this.prismaService.m_admin_department.update({
        where: {
          id,
        },
        data: {
          status: true,
          modified_by: user.id,
        },
        select: { ...selectData },
      });
      return { id };
    } catch (error) {
      throw new ConflictException();
    }
  }

  // soft delete department list
  async deleteDepartmentListByIdMethod(id, user) {
    const departmentCheck = await this.checkDepartmentById(id);
    if (departmentCheck.status === false) {
      throw new ConflictException(`${id} already deleted`);
    }
    try {
      await this.prismaService.m_admin_department.update({
        where: {
          id,
        },
        data: {
          status: false,
          modified_by: user.id,
        },
      });
      return { id };
    } catch (error) {
      throw new ConflictException('Already Deleted');
    }
  }

  // check department details present in table or not by Id
  async checkDepartmentById(id: number) {
    const checkDepartment =
      await this.prismaService.m_admin_department.findUnique({
        where: {
          id,
        },
      });
    if (!checkDepartment) {
      throw new NotFoundException(
        `Department id ${id} is not Exist in database`,
      );
    }
    return checkDepartment;
  }

  // fetching data from data base
  async fetchDataFromDatabase(requestedColumns: string[]): Promise<any[]> {
    if (requestedColumns.length > 6) {
      throw new RangeError('Too many columns requested. Maximum allowed is 6');
    }
    const selectObject: Record<string, boolean> = {};
    if (requestedColumns.length > 0) {
      requestedColumns.forEach((column) => {
        selectObject[column] = true;
      });
    }
    const data = await this.prismaService.m_admin_department.findMany({
      select: selectObject,
    });
    return data;
  }

  // excel data print
  async excelDataPrint(requestedColumns, tableName) {
    const data = await this.fetchDataFromDatabase(requestedColumns);
    const workbook = await this.fileGeneration.generateDataInExcelFormate(
      requestedColumns,
      tableName,
      data, // Pass the data obtained from fetchDataFromDatabase
    );
    return workbook;
  }
  // csv data print
  async csvDataPrint(requestedColumns, tableName) {
    const data = await this.fetchDataFromDatabase(requestedColumns);

    const csvWorkbook = await this.fileGeneration.generateCSVData(
      requestedColumns,
      data,
      tableName,
    );
    return csvWorkbook;
  }

  // pdf data print...
  async pdfDataPrint(requestedColumns, tableName) {
    const data = await this.fetchDataFromDatabase(requestedColumns);

    const pdfBuffer = await this.fileGeneration.generateTablePDF(
      data,
      tableName,
      requestedColumns,
    );
    return pdfBuffer;
  }

  // =========================Testing purpose =========================================
  // Excel testing .....
  async excelDataTest(res, requestedColumns, tableName) {
    const data = await this.fetchDataFromDatabase(requestedColumns);
    const workbook = await this.fileGeneration.generateDataInExcelFormate(
      requestedColumns,
      tableName,
      data, // Pass the data obtained from fetchDataFromDatabase
    );
    res.setHeader(
      'Content-Type',
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    );
    res.setHeader(
      'Content-Disposition',
      `attachment; filename=${tableName}.xlsx`,
    );
    const buffer = await workbook.xlsx.writeBuffer();
    const result = res.end(Buffer.from(buffer));
    return result;
  }
}
