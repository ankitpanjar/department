import {
  Body,
  Controller,
  Post,
  Get,
  Param,
  ParseIntPipe,
  Delete,
  Patch,
  UseGuards,
  Query,
  Res,
} from '@nestjs/common';
import { DepartmentService } from './department.service';
import { CreateDepartmentDto } from './dtos/createDepartment.dto';
import { UpdateDepartmentDto } from './dtos/updateDepartment.dto';
import { User } from 'src/common/decorators/user.decorator';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { AuthGuard } from 'src/common/guards/auth.guard';
import { ApiParam } from '@nestjs/swagger';
// import { Module } from '@nestjs/common';
import { SearchDepartmentDto } from './dtos/serarchDepartment.dto';
import { Response } from 'express';
// import { FileGenerationService } from 'src/file-generation/file-generation.service';

@ApiTags('Department List')
@ApiResponse({ status: 403, description: 'Forbidden resource' })
@ApiResponse({ status: 409, description: 'Conflict' })
@ApiResponse({ status: 500, description: 'Internal server error' })
@ApiResponse({ status: 400, description: 'Bad request' })
@ApiBearerAuth('JWT-Auth')
@Controller('admin/department')
export class DepartmentController {
  constructor(
    private readonly departmentService: DepartmentService, // private readonly filegeneration: FileGenerationService,
  ) {}

  // Create Department List User SignIn jwt token requiered.
  @ApiOperation({ summary: 'Create a new Department' })
  @ApiResponse({ status: 201, description: 'Successful response' })
  @UseGuards(AuthGuard)
  @Post('createDepartment')
  async createDepartment(@Body() body: CreateDepartmentDto, @User() user) {
    const data = await this.departmentService.createDepartmentMethod(
      body,
      user,
    );
    return data;
  }

  // Get All Department Lists
  @ApiOperation({ summary: 'Get All Department List' })
  @ApiResponse({
    status: 200,
    description: 'Successful retrieval of departments',
  })
  @UseGuards(AuthGuard)
  @Get('getAllDepartmentsList')
  async getAllDepartmentLists(@Query() paginationQuery: SearchDepartmentDto) {
    const data = await this.departmentService.getAllDepartmentListsMethod(
      paginationQuery,
    );
    return data;
  }

  // Get Department List By Id
  @ApiOperation({ summary: 'Get Department By Id' })
  @ApiResponse({ status: 200, description: 'Successful response' })
  @UseGuards(AuthGuard)
  @Get('getDepartmentListById/:id')
  async getDepartmentListById(@Param('id', ParseIntPipe) id: number) {
    const data = await this.departmentService.getDepartmentListByidMethod(id);
    return data;
  }

  // update Department List by Id
  @ApiOperation({ summary: 'Update Department By Id' })
  @ApiResponse({ status: 200, description: 'Successful response' })
  @ApiParam({ name: 'id', type: 'integer' })
  @UseGuards(AuthGuard)
  @Patch('updateDepartmentListById/:id')
  async updateDepartmentListById(
    @Param('id', ParseIntPipe) id: number,
    @Body() body: UpdateDepartmentDto,
    @User() user,
  ) {
    const data = await this.departmentService.updateDepartmentListByIdMethod(
      id,
      body,
      user,
    );
    return data;
  }

  // soft Delete Department List
  @ApiOperation({ summary: 'Inactivate Department By Id' })
  @ApiParam({ name: 'id', type: 'integer', description: 'Department ID' })
  @ApiResponse({ status: 200, description: 'Successful response' })
  @UseGuards(AuthGuard)
  @Delete('deleteDepartmentListById/:id')
  async deleteDepartmentListById(
    @Param('id', ParseIntPipe) id: number,
    @User() user,
  ) {
    const data = await this.departmentService.deleteDepartmentListByIdMethod(
      id,
      user,
    );
    return data;
  }

  // activate Department List
  @ApiOperation({ summary: 'Activate Department By Id' })
  @ApiParam({ name: 'id', type: 'integer', description: 'Department ID' })
  @ApiResponse({ status: 200, description: 'Successful response' })
  @UseGuards(AuthGuard)
  @Patch('activateDepartmentList/:id')
  async activateDepartmentListById(
    @Param('id', ParseIntPipe) id: number,
    @User() user,
  ): Promise<{ id: number }> {
    const data = await this.departmentService.activateDepartmentListByIdMethod(
      id,
      user,
    );
    return data;
  }

  // download department in pdf formate
  @ApiOperation({ summary: 'Download Department on PDF formate' })
  @ApiResponse({ status: 200, description: 'Successful response' })
  // @UseGuards(AuthGuard)
  @Get('downloadPdf')
  async downloadPdf(
    @Res() res: Response,
    @Query('columns') columns: string,
  ): Promise<void> {
    try {
      const requestedColumns = columns ? columns.split(',') : [];
      const tableName = 'Department';
      const departmentPdf = await this.departmentService.pdfDataPrint(
        requestedColumns,
        tableName,
      );

      res.setHeader('Content-Type', 'application/pdf');
      res.setHeader(
        'Content-Disposition',
        `attachment; filename=${tableName}.pdf`,
      );
      res.send(departmentPdf);
    } catch (error) {
      console.error(error);
      res.status(500).send('Error generating PDF');
    }
  }

  // download in excel formate
  @ApiOperation({ summary: 'Download Department on Excel formate' })
  @ApiResponse({ status: 200, description: 'Successful response' })
  // @UseGuards(AuthGuard)
  @Get('downloadExcel')
  async downloadExcel(
    @Res() res: Response,
    @Query('columns') columns: string,
  ): Promise<void> {
    try {
      const tableName = 'Department';
      const requestedColumns = columns ? columns.split(',') : [];

      const departmentExcel = await this.departmentService.excelDataPrint(
        requestedColumns,
        tableName,
      );
      res.setHeader(
        'Content-Type',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      );
      res.setHeader(
        'Content-Disposition',
        `attachment; filename=${tableName}.xlsx`,
      );

      const buffer = await departmentExcel.xlsx.writeBuffer();
      res.end(Buffer.from(buffer));
    } catch (error) {
      console.error(error);
      res.status(500).send('Error generating Excel file');
    }
  }

  // download Csv file
  @ApiOperation({ summary: 'Download Department on Csv formate' })
  @ApiResponse({ status: 200, description: 'Successful response' })
  // @UseGuards(AuthGuard)
  @Get('downloadCsv')
  async downloadCSV(
    @Res() res: Response,
    @Query('columns') columns: string,
  ): Promise<void> {
    try {
      const tableName = 'Department';
      const requestedColumns = columns ? columns.split(',') : [];
      const csvPrint = await this.departmentService.csvDataPrint(
        requestedColumns,
        tableName,
      );
      res.setHeader('Content-Type', 'text/csv');
      res.setHeader(
        'Content-Disposition',
        `attachment; filename=${tableName}.csv`,
      );
      res.send(csvPrint);
    } catch (error) {
      console.error(error);
      res.status(500).send('Error generating CSV file');
    }
  }
  // =================== Testing purpose =========================
  // Excel testing .....
  @Get('testExcel')
  async downloadExcelTest(
    @Res() res: Response,
    @Query('columns') columns: string,
  ): Promise<void> {
    try {
      const tableName = 'Department';
      const requestedColumns = columns ? columns.split(',') : [];

      await this.departmentService.excelDataTest(
        res,
        requestedColumns,
        tableName,
      );
    } catch (error) {
      res.status(500).send('Error generating Excel file');
    }
  }
}
