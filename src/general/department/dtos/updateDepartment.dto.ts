import { ApiProperty } from '@nestjs/swagger';
import {
  IsNotEmpty,
  IsOptional,
  IsString,
  Length,
  Matches,
} from 'class-validator';

export class UpdateDepartmentDto {
  @ApiProperty({
    description: 'Enter Department Name',
    example: 'Department name',
    required: false,
  })
  @IsString()
  @IsOptional()
  @Length(3, 50, { message: 'Name must be between 3 and 50 characters' })
  @Matches(/^(?!^\d+$)^[A-Za-z0-9.'-()_+&@]+( [A-Za-z0-9.'-()_+&@]+)*$/, {
    message: 'Invalid characters in name, and it cannot be all numbers',
  })
  name: string;

  @ApiProperty({
    description: 'Enter Description',
    example: 'Description',
    required: false,
  })
  @IsString()
  @IsOptional()
  @IsNotEmpty()
  description: string;

  @ApiProperty({
    description: 'Enter Code ',
    example: 'code',
    required: false,
  })
  @IsString()
  @IsOptional()
  @Length(5, 15, { message: 'Name must be between 5 and 15 characters' })
  @Matches(/^[A-Za-z0-9\-\/\\_]+[A-Za-z0-9\-\/\\_]*[A-Za-z0-9\-\/\\_]$/, {
    message: 'Invalid characters in code',
  })
  code: string;
}
