import { IsNotEmpty, IsString, Matches, Length } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateDepartmentDto {
  @ApiProperty({
    description: 'Enter Department Name',
    example: 'Department name',
    required: true,
  })
  @IsString()
  @IsNotEmpty()
  @Length(3, 50, { message: 'Name must be between 3 and 50 characters' })
  @Matches(/^(?!^\d+$)^[A-Za-z0-9.'-()_+&@]+( [A-Za-z0-9.'-()_+&@]+)*$/, {
    message: 'Invalid characters in name, and it cannot be all numbers',
  })
  name: string;

  @ApiProperty({
    description: 'Enter Description',
    example: 'Description',
    required: false,
  })
  @IsString()
  @IsNotEmpty()
  description: string;

  @ApiProperty({
    description: 'Enter Code',
    example: 'code',
    required: true,
  })
  @IsString()
  @IsNotEmpty()
  @Length(5, 15, { message: 'Name must be between 5 and 15 characters' })
  @Matches(/^[A-Za-z0-9\-\/\\_]+[A-Za-z0-9\-\/\\_]*[A-Za-z0-9\-\/\\_]$/, {
    message: 'Invalid characters in code',
  })
  code: string;
}
