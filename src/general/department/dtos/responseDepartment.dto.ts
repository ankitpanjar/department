import { Exclude, Expose } from 'class-transformer';
export class ResponseDepartmentDto {
  id: number;
  name: string;
  description: string;
  code: string;
  status: boolean;
  created_by: number;
  modified_by: number;
  created_on: Date;
  modified_on: Date;

  @Exclude()
  created_user: {
    username: string;
  };

  @Exclude()
  modified_user: {
    username: string;
  };

  @Expose({ name: 'created_by_username' })
  transformCreatedUsername() {
    return this.created_user.username;
  }

  @Expose({ name: 'modified_by_username' })
  transformModifiedUsername() {
    return this.modified_user.username;
  }

  constructor(partial: Partial<ResponseDepartmentDto>) {
    Object.assign(this, partial);
  }
}
