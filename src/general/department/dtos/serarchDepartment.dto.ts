import { ApiProperty } from '@nestjs/swagger';
import { IsObject, IsOptional, IsPositive, IsString } from 'class-validator';

export class SearchDepartmentDto {
  @ApiProperty({ required: false, type: Number, description: 'page No' })
  @IsOptional()
  @IsPositive()
  offset?: number;

  @ApiProperty({ required: false, type: Number })
  @IsOptional()
  limit?: string;

  @ApiProperty({ required: false, type: String })
  @IsOptional()
  @IsString()
  search?: string;

  @ApiProperty({ required: false, type: String })
  @IsOptional()
  @IsString()
  column_sort?: string;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsString()
  sort_order?: 'ASC' | 'DESC';

  @IsOptional()
  @IsObject()
  advanceFilter: object;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsString()
  name: string;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsString()
  description: string;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsString()
  code: string;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsString()
  status: string;
  // @ApiProperty({ required: false })
  // @Expose()
  // @Transform(({ value }) => value === '1')
  // @IsIn(['0', '1'])
  // status: string;
}
