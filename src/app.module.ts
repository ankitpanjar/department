import { ClassSerializerInterceptor, Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PrismaModule } from './prisma/prisma.module';
import { DepartmentModule } from './general/department/department.module';
import { AuthenticationModule } from './authentication/authentication.module';
import { UsersModule } from './users/users.module';
import { RedisModule } from './redis/redis.module';
import { JwtService } from '@nestjs/jwt';
import { LoggerService } from './common/logger/logger.service';
import { AuthenticationService } from './authentication/authentication.service';
import { APP_FILTER, APP_INTERCEPTOR } from '@nestjs/core';
import { UserInterceptor } from './common/interceptors/user.interceptor';
import { HttpErrorFilter } from './common/logger/http-error.filter';
import { LoggerInterceptor } from './common/interceptors/logger.interceptor';

@Module({
  imports: [
    PrismaModule,
    DepartmentModule,
    AuthenticationModule,
    UsersModule,
    RedisModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    JwtService,
    LoggerService,
    AuthenticationService,
    { provide: APP_INTERCEPTOR, useClass: UserInterceptor },
    { provide: APP_FILTER, useClass: HttpErrorFilter },
    { provide: APP_INTERCEPTOR, useClass: LoggerInterceptor },
    { provide: APP_INTERCEPTOR, useClass: ClassSerializerInterceptor },
  ],
})
export class AppModule {}
