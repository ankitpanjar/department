import { Controller, Post, Body, Req, HttpCode, Get } from '@nestjs/common';
import { AuthenticationService } from './authentication.service';
import { Public } from 'src/common/decorators/public.decorator';
import { User } from 'src/common/decorators/user.decorator';
import { Request } from 'express';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { SignInDto } from './authDto/authdto';

@ApiTags('User Authentication')
@Controller('authentication')
export class AuthenticationController {
  constructor(private readonly authService: AuthenticationService) {}

  @HttpCode(200)
  @Post('signin')
  async signin(@Body() body: SignInDto) {
    const user = await this.authService.signIn(body);
    return user;
  }

  @ApiBearerAuth('JWT-Auth')
  @Public()
  @HttpCode(200)
  @Get('refresh')
  refresh(@Req() req: Request, @User() user) {
    const refreshToken = req.headers?.authorization?.split(' ')[1];
    return this.authService.refresh(user.username, refreshToken);
  }
}
