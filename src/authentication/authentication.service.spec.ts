import { Test, TestingModule } from '@nestjs/testing';
import { AuthenticationService } from './authentication.service';
// import { PrismaService } from './prisma.service'; // Import the PrismaService
import { JwtService } from '@nestjs/jwt';
import { RedisService } from 'src/redis/redis.service'; // Import the RedisService
import { PrismaService } from 'src/prisma/prisma.service';

// Create a mock provider for PrismaService
const prismaServiceMock = {
  user_auth: {
    findUnique: jest.fn(),
    // Mock other methods as needed
  },
};

describe('AuthenticationService', () => {
  let service: AuthenticationService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthenticationService,
        {
          provide: PrismaService, // Provide PrismaService
          useValue: prismaServiceMock,
        },
        JwtService,
        RedisService,
      ],
    }).compile();

    service = module.get<AuthenticationService>(AuthenticationService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should test a method', async () => {
    // Example: Mocking the findUnique method for a specific test case
    prismaServiceMock.user_auth.findUnique.mockResolvedValue({
      id: 1,
      username: 'testuser',
      // Other properties as needed
    });

    // Your test logic here
  });
});
