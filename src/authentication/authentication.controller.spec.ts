import { Test, TestingModule } from '@nestjs/testing';
import { AuthenticationController } from './authentication.controller';
import { AuthenticationService } from './authentication.service';
import { SignInDto } from './authDto/authdto';
import { Request } from 'express';
import { RedisModule } from 'src/redis/redis.module';
import { RedisService } from 'src/redis/redis.service';
import { PrismaService } from 'src/prisma/prisma.service';
import { PrismaModule } from 'src/prisma/prisma.module';

describe('AuthenticationController', () => {
  let controller: AuthenticationController;
  let authService: AuthenticationService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [RedisModule, PrismaModule],
      controllers: [AuthenticationController],
      providers: [AuthenticationService, RedisService, PrismaService],
    }).compile();

    controller = module.get<AuthenticationController>(AuthenticationController);
    authService = module.get<AuthenticationService>(AuthenticationService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('signin', () => {
    it('should sign in a user', async () => {
      const signInDto: SignInDto = {
        username: 'ankitpanjar@gmail.com',
        password: '1234',
      };
      const user = {
        data: {
          id: 1, // Replace with the actual user ID
          username: 'testuser', // Replace with the actual username
          backendTokens: {
            access_token: 'access_token_value',
            refresh_token: 'refresh_token_value',
            expiresIn: 60,
          },
        },
        /* create a mock user object or use a testing library like Jest Mocks */
      };
      jest.spyOn(authService, 'signIn').mockResolvedValue(user);

      const result = await controller.signin(signInDto);

      expect(result).toBe(user);
    });
  });

  describe('refresh', () => {
    it('should refresh the user token', async () => {
      it('should refresh the user token', async () => {
        const req: Request = {
          headers: {
            authorization: 'Bearer token',
          },
          get: (name: string) => {
            if (name === 'authorization') {
              return 'Bearer token';
            }
            // Handle other headers if needed
            return ''; // Return empty string for headers not handled
          },
        } as Request;

        const user = {
          // create a mock user object
          username: 'testuser',
        };

        const refreshToken = 'token';

        const tokens = {
          access_token: 'access_token_value',
          refresh_token: 'refresh_token_value',
          expiresIn: 60,
        };

        jest.spyOn(authService, 'refresh').mockResolvedValue(tokens);

        const result = await controller.refresh(req, user);

        expect(result).toEqual(tokens);
        expect(authService.refresh).toHaveBeenCalledWith(
          user.username,
          refreshToken,
        );
      });
    });
  });
});
