import {
  Injectable,
  UnauthorizedException,
  NotFoundException,
  ForbiddenException,
} from '@nestjs/common';
import { Tokens } from './types/dto.type';
import { JwtService } from '@nestjs/jwt';
import * as jwt from 'jsonwebtoken';
import { PrismaService } from 'src/prisma/prisma.service';
import * as bcrypt from 'bcrypt';
import { RedisService } from 'src/redis/redis.service';
import { SignInDto } from './authDto/authdto';

@Injectable()
export class AuthenticationService {
  constructor(
    private readonly userRepository: PrismaService,
    private readonly jwtService: JwtService,
    private readonly redisService: RedisService,
  ) {}

  async signIn({ username, password }: SignInDto) {
    const user = await this.userRepository.user_auth.findUnique({
      where: { username },
    });
    if (!user) {
      throw new UnauthorizedException('Invalid credentials');
    }
    const isPasswordValid = await bcrypt.compare(password, user.password);
    if (!isPasswordValid) {
      throw new UnauthorizedException('Invalid credentials');
    }
    const tokens = await this.generateTokens(user.id, username, user.password);
    await this.redisService.saveJwtToken(username, tokens.refresh_token);
    await this.updateRefreshToken(username, tokens.refresh_token);
    const data = {
      user: {
        id: user.id,
        username,
      },

      backendTokens: tokens,
    };
    return data;
  }

  async updateRefreshToken(username: string, refresh_token: string) {
    const hashedToken = await bcrypt.hash(refresh_token, 10);
    const user = await this.userRepository.user_auth.update({
      where: {
        username: username,
      },
      data: {
        refresh_token: hashedToken,
      },
    });
    return { user };
  }

  async generateTokens(
    id: number,
    username: string,
    password: string,
  ): Promise<Tokens> {
    const expiresIn = 60 * 5;
    const [at, rt] = await Promise.all([
      // access token
      this.jwtService.signAsync(
        {
          username,
          id,
        },
        {
          secret: 'at-secret',
          expiresIn: expiresIn, //expire in 1 min
        },
      ),
      // refresh token
      this.jwtService.signAsync(
        {
          username,
          id,
        },
        {
          secret: 'rt-secret',
          expiresIn: expiresIn * 60 * 24, // for one day
        },
      ),
    ]);
    const return_data = {
      access_token: at,
      refresh_token: rt,
      expiresIn: expiresIn,
    };
    return return_data;
  }

  async isTokenExpired(token: string): Promise<boolean> {
    try {
      const decodedToken = (await jwt.verify(token, 'at-secret')) as {
        exp: number;
      };
      const currentTimestamp = Math.floor(Date.now() / 1000);
      return decodedToken.exp < currentTimestamp;
    } catch (error) {
      if (error instanceof jwt.TokenExpiredError) {
        return true;
      }
      return true;
    }
  }

  async refresh(username: string, refresh_token: string) {
    const user = await this.userRepository.user_auth.findUnique({
      where: {
        username: username,
      },
    });
    if (!user)
      throw new NotFoundException({
        message: 'There is no client on this credentials',
      });
    await this.redisService.getRefreshToken(username);
    const rtMatches = await bcrypt.compare(refresh_token, user.refresh_token);
    if (!rtMatches)
      throw new ForbiddenException({ message: "RefreshToken didn't match" });
    const tokens = await this.generateTokens(
      user.id,
      user.username,
      user.password,
    );
    await this.updateRefreshToken(user.username, tokens.refresh_token);
    await this.redisService.saveJwtToken(username, tokens.refresh_token);
    return tokens;
  }
}
