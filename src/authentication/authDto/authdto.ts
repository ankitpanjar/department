import { IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class SignInDto {
  @ApiProperty({
    required: true,
    description: 'User Name ',
    example: 'user name',
  })
  @IsNotEmpty()
  username: string;

  @ApiProperty({
    required: true,
    description: 'User Password',
    example: 'password',
  })
  @IsNotEmpty()
  password: string;
}
